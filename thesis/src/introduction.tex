% !TEX root = thesis.tex

\chapter{Introduction}
\label{ch:introduction}

The higher-dimensional representations described in \refpa{pa:representation} and the operations described in \refpa{pa:operations} can be difficult to implement, especially when we expect these implementations to be fast, robust, generic, compact and dimension-independent.
This is true even when the basic ideas and algorithms are provided, as has been done in this thesis.

For the sake of full reproducibility, this chapter shows some of the key implementation details that are used to efficiently implement the representations and operations described in this thesis.
\refse{se:libraries} lists the main libraries that were used and how they were used.
\refse{se:computerarithmetic} explains the main techniques that are used to perform arithmetic and geometric operations robustly.
Finally, \refse{se:traits} describes the traits programming technique and its use in CGAL and this thesis to produce dimension-independent efficient implementations.

\section{Main libraries used within this thesis}
\label{se:libraries}

\begin{description}

\item[CGAL\footnotemark]
\footnotetext{\url{http://www.cgal.org}}
(the Computational Geometry Algorithms Library) contains a wide variety of 2D/3D/$n$D data structures and computational geometry algorithms.
Some of its basic packages are directly used within this thesis to store and manipulate numbers and basic shapes, namely: Algebraic Foundations, Number Types, 2D and 3D Linear Geometry Kernel, and $d$D Geometry Kernel.
Most significantly, the packages Combinatorial Maps and Linear Cell Complex are used in most of the implementations described in the previous chapters.
Finally, other packages are used as temporary data structures and to process and clean input data: 3D Polyhedral Surface, Halfedge Data Structures, 3D Boolean Operations on Nef Polyhedra, 2D Triangulation, and Principal Component Analysis.
A few other packages are used as dependencies of the aforementioned packages.

\item[GDAL\footnotemark]
\footnotetext{\url{http://www.gdal.org}}
(the Geospatial Data Abstraction Library) reads and writes commonly used GIS file formats.
Within this thesis, its OGR vector module is mainly used to read and write polygons described as well-known text \citep{SimpleFeatures1}, or in Esri Shapefile \citep{Shapefile} and FileGDB files.

\item[IfcOpenShell\footnotemark]
\footnotetext{\url{http://www.ifcopenshell.org}}
is a library that is able to read and write IFC files \citep{ISO16739:2013}.
It internally uses the Open CASCADE geometry types, including to convert implicit geometries (\eg\ those built using constructive-solid geometry or sweeps as in \reffig{fig:ifc}) into explicit ones that can be stored using boundary representation, or to create meshes of a given degree of accuracy from curved surfaces.

\item[Open CASCADE\footnotemark]
\footnotetext{\url{http://www.opencascade.org}}
is a library that is able to manipulate geometric representations in CAD applications.
In theory, it supports complex geometric operations between implicit geometries, including Boolean set operations with 3D point sets.
However, in practice it performs poorly with GIS data, often failing due to numerical errors or imperfect data.

\end{description}
