To experimentally derive a relationship between the formation of boundary layer and the effect of fluid flow over a flat plate, an open return wind tunnel is used.
The wind tunnel used in this experiment is a Gunt Hamburg HM170.

\section{Equipment Description}

\begin{itemize}
\item Air Inlet
      Allows the air to be forced into the test section at the desired speed.
\item Honeycomb flow straightener
      Ensures that the fluid flow velocity is uniform across the cross-sectional area of the inlet, while also minimizing turbulence.
\item Contraction section
      The flow is compressed as it moves through this section. This increases the freestream fluid velocity as it moves into the test section.
\item Test section
      The flat plate is secured at the desired length from its leading edge within this space.
\item Diffuser section
      The flow is allowed to expand so that the fluid velocity is reduced before it is allowed to move out through the outlet of the wind tunnel.
\item Drive section
      A motorized fan is integrated within this section which allows the user to control the freestream fluid flow within the wind tunnel.
\item Air outlet
      Allows the air to move out.
\item Switch box
      Main power switch and control switches are located ithin the switch box.
\item Data logger
      Records data onto the dedicated computer software.
\item Pitot Tube
      Height with respect to the flat plate is varied to study the effect of velocity on the boundary layer formation around the flat plate.
\item Flat plate
      A smooth or a rough plate can be installed in the given arrangement for the experimental analysis.
\item Rotary axial control knob
      The plate distance from the leading edge is adjusted by turning this knob.
\item Micrometre spindle
      Can be either turned clockwise or anti-clockwise to adjust the height of the pitot tube from the flat plate.
\item Micrometre scale
      The height of the pitot tube from the flat plate is shown on the micrometre scale.
\item Main power switch
      Used to turn on/off the test rig.
\item Power ‘on’ switch for the motorized fan unit
      Turns on the fan within the wind tunnel.
\item Wind speed controller
      Freestream fluid velocity can be either increased or decreased by turning this knob clockwise or anti-clockwise respectively.
\item Power ‘off’ switch for the motorized fan unit
      Turns off the fan within the wind tunnel.
\end{itemize}

\section{Description of Data}

\begin{table}
\begin{tabular}{|p{2.2cm}|p{1.3cm}p{1.3cm}p{1.3cm}|p{1.3cm}p{1.3cm}p{1.3cm}|}
\hline
                          & \multicolumn{3}{c}{Smooth Plate}                                         & \multicolumn{3}{c}{Rough Plate}                                       \\

Freestream Velocity       & 10 m/s                 & 18 m/s                 & 22 m/s                 & 10 m/s                & 18 m/s                & 22 m/s                \\
\hline
80 mm from leading edge   & 10 m/s, 80 mm, smooth  & 18 m/s, 80 mm, smooth  & 22 m/s, 80 mm, smooth  & 10 m/s, 80 mm, rough  & 18 m/s, 80 mm, rough  & 22 m/s, 80 mm, rough  \\
120 mm from leading edge  & 10 m/s, 120 mm, smooth & 18 m/s, 120 mm, smooth & 22 m/s, 120 mm, smooth & 10 m/s, 120 mm, rough & 18 m/s, 120 mm, rough & 22 m/s, 120 mm, rough \\
160 mm from leading edge  & 10 m/s, 160 mm, smooth & 18 m/s, 160 mm, smooth & 22 m/s, 160 mm, smooth & 10 m/s, 160 mm, rough & 18 m/s, 160 mm, rough & 22 m/s, 160 mm, rough \\
\hline
\end{tabular}
\end{table}

\subsection{Apparatus Calibration}
\begin{enumerate}
\item The digital Instrumentation devices are ensured to be in working order.
\item The power button on the anemometer is pressed to switch it on.
\begin{itemize}
      \item The device is allowed to run for a few seconds until the values stop fluctuating.
      \item The zero-calibration button is then held for 5 seconds to zero the value of fluid flow while the wind tunnel is non-operational.
\end{itemize}
\end{enumerate}

\subsection{System Setup}
\begin{enumerate}
\item The smooth plate is first installed within the test section of the wind tunnel.
\item The plate is secured within the test section such that its leading edge is in line with the tip of the pitot tube (i.e., at 0 mm).
\item The pitot tube is adjusted to be at maximum height with respect to the flat plate.
\item The HD350 software interface is launched.
\end{enumerate}

\subsection{System Operation}
\begin{enumerate}
\item The main power switch on the switch box is turned such that the test rig switches on.
\item The ‘on’ power switch for the motorized fan is pressed such that the wind tunnel becomes operational.
\item The wind speed controller is used to adjust the freestream fluid velocity to be at 10 m/s.
\item The flat plate is adjusted to be in line with the tip of the pitot tube at a length of 80 mm from the leading edge of the flat plate.
\item The height of the pitot tube with respect to the flat plate is adjusted such that the fluid velocity is shown to be 10 m/s.
\end{enumerate}

\subsection{Data collection}
\begin{enumerate}
\item The start button on the software interface is clicked and the recording frequency is set to 1 second.
\item The height value is read through the scale on the micrometre and is recorded.
\item The system is allowed to run for 30 seconds.
\item The stop button on the software interface is clicked to terminate the data collection.
\item The file is saved for the recorded height from the flat plate, at the recorded distance from leading edge.

\item The pitot tube height is then decreased in decrements of 0.5 mm until the distance between the tip
and the plate is minimum, and steps 12-16 are repeated.
\item The flat plate is then adjusted to be in line with the tip of the pitot tube at a length of 120 mm and
then at a length of 160 mm from the leading edge of the flat plate, and steps 11-17 are repeated at
these two lengths from the leading edge.
\item The wind speed controller is used to adjust the freestream fluid velocity at 18 m/s and then at 22 m/s, and steps 10-18 are repeated at these two freestream velocities.
\item The ‘off’ power switch for the motorized fan is pressed such that the wind tunnel becomes non-operational.
\item The smooth plate is replaced with the rough plate, and step 2B is repeated. Furthermore, steps 4-20 are repeated, and the required data is obtained.
\end{enumerate}

\section{Experimental Results}
\begin{tabular}{ |p|ppp|ppp|  }
\hline
                              &       & \multicolumn{6}{c}{Plate Type} \\
\hline
                              &       & \multicolumn{3}{c}{Smooth Plate} & \multicolumn{3}{c}{Rough Plate} \\

Free Stream Velocity          &       & 10 m/s   & 18 m/s   & 22 m/s   & 10 m/s   & 18 m/s   & 22 m/s   \\
\hline
\multirow{Distance from Leading Edge} & 80 mm  & Figure 1 & Figure 2 & Figure 3 & Figure 4 & Figure 5 & Figure 6 \\
                                      & 120 mm & Figure 1 & Figure 2 & Figure 3 & Figure 4 & Figure 5 & Figure 6 \\
                                      & 160 mm & Figure 1 & Figure 2 & Figure 3 & Figure 4 & Figure 5 & Figure 6 \\
\hline
\end{tabular}
