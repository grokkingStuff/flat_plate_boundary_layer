All objects immersed within a viscous fluid will experience a retarding force, similar to friction between two solid bodies in contact, due to the interaction between the object surface and the fluid surrounding it.

In the region near the object's surface, the fluid has lower velocity and appear to change quite rapidly from its freestream velocity to zero. The fluid velocity at any point on the object's surface is zero relative to the surface (known as the no-slip condition).

The flow of the fluid can either be turbulent (irregular flow) or laminar (smooth flow with minimal eddy formation), and is treated by proxy with the Reynold's number.

\section{Simplifying Conservation Laws into Boundary Layer Equations}
The fluid continuity equation describes the conversation of mass in differential form while the Navier-Stokes equations describe the conservation of momentum in differential form.

\begin{equation}
\pde{u}{t} + u\pde{u}{t} + v\pde{u}{y} + w\pde{u}{z} = -\frac{1}{\rho}\pde{p}{x} + g_{x} + \nu\left( \pdetwo{u}{x} + \pdetwo{u}{y} + \pdetwo{u}{z} \right) \label{eq:3d_navier_stokes_x} \\
\end{equation}

\begin{equation}
\pde{v}{t} + u\pde{v}{t} + v\pde{v}{y} + w\pde{v}{z} = -\frac{1}{\rho}\pde{p}{y} + g_{y} + \nu\left( \pdetwo{v}{x} + \pdetwo{v}{y} + \pdetwo{v}{z} \right) \label{eq:3d_navier_stokes_y} \\
\end{equation}

\begin{equation}
\pde{w}{t} + u\pde{w}{t} + v\pde{w}{y} + w\pde{w}{z} = -\frac{1}{\rho}\pde{p}{z} + g_{z} + \nu\left( \pdetwo{w}{x} + \pdetwo{w}{y} + \pdetwo{w}{z} \right) \label{eq:3d_navier_stokes_z} \\
\end{equation}

\begin{equation}
\pde{u}{x} + \pde{v}{y} + \pde{w}{z} = 0 \label{eq:3d_navier_stokes_continuity}
\end{equation}

By using suitable assumptions, equations (\ref{eq:3d_navier_stokes_x}, \ref{eq:3d_navier_stokes_y}, \ref{eq:3d_navier_stokes_z}, \ref{eq:3d_navier_stokes_continuity}) can be simplified.

\begin{description}
\item Steady state assumption

      A flow field is said to be a steady state when the flow at any position in space does not change in time. Any differential term based on time $t$ can be simplified to zero.

\begin{align*}
\pde{u}{t} &= 0 & \pde{v}{t} &= 0 \\
\pde{w}{t} &= 0 & \pde{p}{t} &= 0 \\
\end{align*}

\item Neglect gravity terms

      A flow field is said to be a steady state when the flow at any position in space does not change in time. Any differential term based on time $t$ can be simplified to zero.

\begin{align*}
\vec{g} &= 0 & \pde{g}{x} &= 0 \\
\pde{g}{y} &= 0 & \pde{g}{z} &= 0 \\
\end{align*}

\item Two-dimensional simplification

      While any real-life flowfield is three-dimensional, we can simplify our analysis by assuming that cross sections of the field are identical to each other with no transverse fluid velocity. In this analysis, we will be using the $x$-coordinate as the direction of the freestream velocity and $z$-coordinate as the height above the flate plate.

\begin{align*}
v &= 0 & \pde{v}{x} &= 0 \\
\pde{v}{y} &= 0 & \pde{v}{z} &= 0 \\
\pde{u}{y} &= 0 & \pde{w}{z} &= 0 \\
\pdetwo{v}{y} &= 0 & \pdetwo{v}{z} &= 0 \\
\pdetwo{u}{y} &= 0 & \pdetwo{w}{z} &= 0 \\
\end{align*}

\item Flow is parallel to the plate

      The flow is primarily parallel to the plate.

\begin{align*}
w &= 0 & \pde{w}{t} &= 0 \\
\pde{w}{x} &= 0 & \pde{w}{z} &= 0 \\
\pdetwo{w}{x} &= 0 & \pdetwo{w}{z} &= 0 \\
\end{align*}

\item Thin boundary layer assumption

      Prandtl's assumption of a thin boundary layer allows for his use of similarity parameters (non-dimensional analysis for flowsfields in which streamline patterns are geometrically similar).

\begin{align*}
x' &= \frac{ x }{ L         } & y' &= \frac{ y }{ \delta    } \\
u' &= \frac{ u }{ U         } & p' &= \frac{ p }{ \rho {U_{\infty}}^{2} } \\
\end{align*}

\end{description}

\begin{align*}
\cancel{\pde{u}{t}} + u\pde{u}{x} + \cancel{v\pde{u}{y}} + \cancel{w\pde{u}{z}} &= -\frac{1}{\rho}\pde{p}{x} + \cancel{g_{x}} + \nu\left( \pdetwo{u}{x} + \cancel{\pdetwo{u}{y}} + \pdetwo{u}{z} \right) \\
\Rightarrow u\pde{u}{t} &= -\frac{1}{\rho}\pde{p}{x} + \nu\pdetwo{u}{x} + \nu\pdetwo{u}{z}
\end{align*}

\begin{align*}
\cancel{\pde{v}{t}} + \cancel{u\pde{v}{x}} + \cancel{v\pde{v}{y}} + \cancel{w\pde{v}{z}} &= \cancel{-\frac{1}{\rho}\pde{p}{y}} + \cancel{g_{y}} + \nu\left( \cancel{\pdetwo{v}{x}} + \cancel{\pdetwo{v}{y}} + \cancel{\pdetwo{v}{z}} \right) \\
\Rightarrow 0 &= 0
\end{align*}

\begin{align*}
\pde{w}{t} + u\pde{w}{x} + \cancel{v\pde{w}{y}} + w\pde{w}{z} &= -\frac{1}{\rho}\pde{p}{z} + \cancel{g_{z}} + \nu\left( \pdetwo{w}{x} + \cancel{\pdetwo{w}{y}} + \pdetwo{w}{z} \right) \\
\Rightarrow \pde{w}{t} + u\pde{w}{t} + w\pde{w}{z} &= -\frac{1}{\rho}\pde{p}{z} + \nu\pdetwo{w}{x} + \nu\pdetwo{w}{z}
\end{align*}

\begin{align*}
\pde{u}{x} + \cancel{\pde{v}{y}} + \pde{w}{z} &= 0 \\
\Rightarrow \pde{u}{x} + \pde{w}{z} &= 0
\end{align*}

The full equation of motion for a two-dimensional flow is:

\marginpar{
\captionsetup{type=table}
\centering
\caption[Symbols \& meaning in Navier-Stokes equations]{Symbols \& meaning in Navier-Stokes equations}
\label{tab:navier_stokes_symbols}
\small
\begin{tabular}{cc}
\toprule
Symbol & Meaning \\
\midrule
$x$ & horizontal coordinate \\
$y$ & vertical coordinate \\
$u$ & horizontal fluid velocity \\
$v$ & vertical fluid velocity \\
$p$ & fluid pressure \\
\bottomrule
\end{tabular}
}

\begin{align}
u\pde{u}{x} + w\pde{u}{z} = -\frac{1}{\rho}\pde{p}{x} + \nu\left( \pdetwo{u}{x} + \pdetwo{u}{y} \right)
\pde{u}{x} + \pde{w}{z} = 0
\end{align}

\begin{equation}
\pde{u'}{t} +
u^{\prime} \pde{ u^{\prime} }{x} +
v^{\prime} \pde{ u^{\prime} }{y}
=
-\pde{ p^{\prime} }{ x^{\prime} } +
\frac{\nu}{UL} \pdetwo{ u^{\prime} }{ x^{\prime} } +
\frac{\nu}{UL} \frac{ L^{2} }{ \delta^{2} } \pdetwo{ u^{\prime} }{ y^{\prime} }

\label{eq:navier_stokes_u}
\end{equation}

\section{Types of boundary layer thickness}
Fluid velocity is zero at the flat plate surface \footnote{No-slip condition} and is equal to the free-stream velocity at infinity away from the surface \footnote{Infinity away from the surface is best thought of as distance away from the plate at which the fluid is not significantly affected by the presence of the plate}.

\subsection{Boundary Thickness}

The boundary thickness, $\delta$, is defined as the height at which the particle's velocity is 99\% of the free stream velocity.

\begin{equation}
\delta = \int \mathrm d y
\end{equation}

\subsection{Momentum Thickness}

The momentum thickness describes the boundary layer in terms of momentum flux, making it useful in determining skin friction drag. \footnote{skin friction drag is the viscous force that causes the loss of momentum. By slowing down the fluid (adding force in the $-x$-direction, the flat plate experiences a force in the $+x$-direction.)}

The momentum thickness represents the height of the free-stream flow which would be needed to make up the deficiency in momentum flux within the boundary layer due to the shear force at the surface.

The momentum thickness for an in-compressible boundary layer is given by:

\begin{align*}
\rho b  {U_{\infty}}^{2} \theta &= \rho b \int u \left( U_{\infty} - u \right) \mathrm d y \\
\theta &= \int \frac{u}{u_{\infty}} \left( 1 - \frac{u}{u_{\infty}} \right) \mathrm d y
\end{align*}
