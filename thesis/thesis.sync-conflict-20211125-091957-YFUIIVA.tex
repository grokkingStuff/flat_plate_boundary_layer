\documentclass[parskip=half,
               fontsize=12pt,
               % chapterprefix=true,
               numbers=noenddot,
               bibliography=totoc]{scrbook}

% For printing on A4
\usepackage[includemp,
            a4paper,
            layoutwidth=18.90cm,
            layoutheight=24.58cm,
            layouthoffset=1.05cm,
            layoutvoffset=2.56cm,
            top=2.170cm,
            bottom=3.510cm,
            inner=1.668cm,
            outer=2.699cm,
            marginparwidth=4cm, % Fixed for now
            marginparsep=0.4cm]{geometry}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{unicode-math} %only usable in xetex and luatex

\usepackage[makeroom]{cancel}

\usepackage[main=english]{babel} %Correct hyphenation

\usepackage{xcolor}                 % Colours in text

\usepackage{fontspec}               % Selecting fonts

\usepackage{newtxtext,newtxmath}
\usepackage{inconsolata}
%\usepackage{unicode-math}

\setmainfont{Times New Roman}
\setmathfont{Asana Math}
\setsansfont{Trebuchet MS}
\setmonofont{Inconsolata}

% Font setup
%\setmainfont{Times New Roman}
%\setmonofont{Courier New}
% \newfontfamily\fanciestfont[Ligatures={TeX,Discretionary}]{Feijoa-Display}
% \newfontfamily\fancyfont[Ligatures=TeX]{Feijoa-Display}
% \newfontfamily [Ligatures=TeX]{Times New Roman}
\addtokomafont{disposition}{\normalfont}
\addtokomafont{title}{\normalfont}
\addtokomafont{chapter}{\normalfont}
\addtokomafont{chapterentry}{\normalfont\scshape}
\addtokomafont{descriptionlabel}{\normalfont\bfseries}
\addtokomafont{caption}{\footnotesize}

\PassOptionsToPackage{hyphens}{url}\usepackage[unicode,
            breaklinks=true,
            colorlinks=true,
            allcolors=greenforlinks,
            pagebackref,
            linktoc=all,
            pdftitle={Higher-dimensional modelling of geographic information},
            pdfauthor={Ken Arroyo Ohori}]{hyperref}  % Hyperlinks

\usepackage{bookmark}               % Add things in TOC
% \usepackage{showframe}              % Frames to easily set up page layout
\usepackage[hypcap=true]{caption}   % Correctly placed anchors for hyperlinks
\usepackage{floatrow}               % Set up captions of floats
\usepackage{marginfix}              % Make marginpars float freely
\usepackage{metalogo}               % XeTeX logo
\usepackage{scrlayer-scrpage}       % Customise head and foot regions
\usepackage[footnote]{snotez}       % Footnotes as sidenotes
% \usepackage{showlabels}             % Show labels
\usepackage[linesnumbered,
            ruled,
            vlined]{algorithm2e}    % Algorithms
\usepackage{multirow}               % Cells occupying multiple rows in tables
\usepackage{multicol}               % Multiple columns in dictionary
\setlength\columnseprule{.4pt}
\usepackage[square]{natbib}         % Bibliography management

% More floats
\extrafloats{100}

\usepackage{pdfpages}               % Insert PDF pages

\definecolor{greenforlinks}{rgb}{0.09, 0.45, 0.27}

%\usepackage{tocloft}                % Customise table of contents
% not recommended to be used in scrbook

\usepackage{subfig}                 % Subfigures

\usepackage{booktabs}               % Nicer tables

\renewcaptionname{english}{\listfigurename}{Figures}

\renewcaptionname{english}{\listtablename}{Tables}

% Figure captions with no indentation
\setcapindent{0pt}

% Levels    in table of contents
\setcounter{tocdepth}{1}

% Page styles for tables of contents, figures and tables
\renewpagestyle{scrheadings}{
  {\makebox[2em][r]{\thepage}\quad\rule{1pt}{100pt}\quad{}Contents}%
  {\hfill\rightmark\quad\rule{1pt}{100pt}\quad\makebox[2em][l]{\thepage}}%
  {}
}{
  {}%
  {}%
  {}
}
\renewpagestyle{plain.scrheadings}{
  {}%
  {}%
  {}
}{
  {\thepage}%
  {\hfill\thepage}%
  {}
}

\usepackage{listings}

% Figures and tables
\floatsetup[figure]{margins=hangoutside,
                    facing=yes,
                    capposition=beside,
                    capbesideposition={center,outside},
                    floatwidth=\textwidth}
\floatsetup[widefigure]{margins=hangoutside,
                        facing=yes,
                        capposition=bottom}
\floatsetup[table]{margins=hangoutside,
                   facing=yes,
                   capposition=beside,
                   capbesideposition={center,outside},
                   floatwidth=\textwidth}
\floatsetup[widetable]{margins=hangoutside,
                       facing=yes,
                       capposition=bottom}

% Listings  code
\definecolor{listingkeywords}{rgb}{0.00, 0.5, 0.0}
\definecolor{listingidentifiers}{rgb}{0, 0, 0}
\definecolor{listingcomments}{rgb}{0.25, 0.5, 0.5}
\definecolor{listingstrings}{rgb}{0.73, 0.13, 0.13}
\definecolor{listingnumbers}{rgb}{0.25, 0.25, 0.25}
\lstset{
  basicstyle=\scriptsize\ttfamily,
  keywordstyle=\color{listingkeywords}\bfseries,
  identifierstyle=\color{listingidentifiers},
  commentstyle=\color{listingcomments}\itshape,
  stringstyle=\color{listingstrings},
  numberstyle=\scriptsize\color{listingnumbers}\ttfamily,
  frame=single,
  tabsize=4,
  language=[ISO]C++
}

\usepackage{ccicons}                % Creative Commons icons

\usepackage{etoolbox}               % Easy programming to modify TeX stuff

% Bullets
\renewcommand{\labelitemi}{►}

% Sidenotes
\setsidenotes{text-mark-format=\textsuperscript{\normalfont#1},
              note-mark-format=#1:,
              note-mark-sep=\enskip}

% Part  title style [todo]
\addtokomafont{part}{\Huge}
\renewcommand*{\partformat}{\vspace{-6cm}\partname~\thepart\autodot\thispagestyle{empty}}

% Chapter   title style
\makeatletter
\setlength{\fboxsep}{0cm}
\renewcommand*{\@@makechapterhead}[1]{%
  \vspace*{3\baselineskip plus \parskip}
  \makebox{%
    \makebox[\linewidth]{\parbox[c][2cm]{\linewidth}{\if@mainmatter\raggedleft\fi\size@chapter{#1}}}%
    \if@mainmatter%
      \makebox[\marginparsep]{\parbox[c][2cm]{\marginparsep}{\centering\rule{1pt}{2cm}}}%
      \makebox[4cm]{\parbox[c][2cm]{4cm}{\scalebox{5}{ \thechapter\autodot}}}%
    \fi
  }%
  \vspace*{1.5\baselineskip plus .1\baselineskip minus .167\baselineskip}
}
\makeatother

% Formatting of back references
\renewcommand*{\backref}[1]{}
\renewcommand*{\backrefalt}[4]{{%
\ifcase#1 Not cited.%
\or{}Cited on page~#2.%
\else Cited on pages~#2.%
\fi%
}}

% Custom    commands
\newcommand{\ie}{i.e.}
\newcommand{\eg}{e.g.}
\newcommand{\cf}{cf.}
\newcommand{\refpa}[1]{\hyperref[#1]{Part}~\ref{#1}}
\newcommand{\refch}[1]{\hyperref[#1]{Chapter}~\ref{#1}}
\newcommand{\refchs}[1]{Chapters~\ref{#1}}
\newcommand{\refse}[1]{\hyperref[#1]{\S}\ref{#1}}
% \newcommand{\refses}[1]{\S\ref{#1}}
\newcommand{\refap}[1]{\hyperref[#1]{Appendix}~\ref{#1}}
\newcommand{\reffig}[1]{\hyperref[#1]{Figure}~\ref{#1}}
\newcommand{\reffiges}[1]{\hyperref[#1]{Figura}~\ref{#1}}
\newcommand{\reffignl}[1]{\hyperref[#1]{Figuur}~\ref{#1}}
\newcommand{\reffigs}[1]{Figures~\ref{#1}}
\newcommand{\reffigp}[1]{\hyperref[#1]{Figure}~\ref{#1} on \hyperref[#1]{page}~\pageref{#1}}
\newcommand{\reftab}[1]{\hyperref[#1]{Table}~\ref{#1}}
\newcommand{\refalgo}[1]{\hyperref[#1]{Algorithm}~\ref{#1}}

% Stuff
%\newcommand{\pp@g@sidenote}{}

% PDE terms
\newcommand{\pde}[2]{ \frac{\partial #1}{\partial #2} }
\newcommand{\pdetwo}[2]{ \frac{\partial^{2} #1}{\partial {#2}^{2} }}

% \nofiles%
\begin{document}

% Front cover
%\includepdf{cover-front.pdf}

% Sloppy spacing works better for small paper sizes: better than text outside margin, esp. because of marginpars
\sloppy

\frontmatter

\newgeometry{top=2.170cm,
            bottom=3.510cm,
            inner=2.1835cm,
            outer=2.1835cm,
            ignoremp}

\input{pre}


% No colour links in tables of contents and list of figures
\clearpage%
\tableofcontents
\listoffigures
% \listoftables
\restoregeometry%

\mainmatter%

% Lengths used for page head
\newlength{\overflowingheadlen}
\setlength{\overflowingheadlen}{\linewidth}
\addtolength{\overflowingheadlen}{\marginparsep}
\addtolength{\overflowingheadlen}{\marginparwidth}

% Page style for preface
\renewpagestyle{scrheadings}{
  {\hspace{-\marginparwidth}\hspace{-\marginparsep}\makebox[\overflowingheadlen][l]{\makebox[2em][r]{\thepage}\quad\rule{1pt}{100pt}\quad{}Preface}}%
  {\makebox[\overflowingheadlen][r]{\rightmark\quad\rule{1pt}{100pt}\quad\makebox[2em][l]{\thepage}}}%
  {}
}{
  {}%
  {}%
  {}
}
\renewpagestyle{plain.scrheadings}{
  {}%
  {}%
  {}
}{
  {\thepage}%
  {\makebox[\overflowingheadlen][r]{\thepage}}%
  {}
}

% Page style for chapters
\renewpagestyle{scrheadings}{
  {\hspace{-\marginparwidth}\hspace{-\marginparsep}\makebox[\overflowingheadlen][l]{\makebox[2em][r]{\thepage}\quad\rule{1pt}{100pt}\quad{}\leftmark}}%
  {\makebox[\overflowingheadlen][r]{\rightmark\quad\rule{1pt}{100pt}\quad\makebox[2em][l]{\thepage}}}%
  {}
}{
  {}%
  {}%
  {}
}
\renewpagestyle{plain.scrheadings}{
  {}%
  {}%
  {}
}{
  {\thepage}%
  {\makebox[\overflowingheadlen][r]{\thepage}}%
  {}
}

\input{src/introduction}
\input{src/theory}
\input{src/conclusions}
\appendix%
\addtocontents{toc}{\medskip\bigskip}
\input{src/implementation}

% \setpartpreamble{
%   \vspace{3cm}
%   \begin{center}
%   \includegraphics[width=0.8\linewidth]{figs/gmaps-3d-simplices}
%   \end{center}
% }
% \newgeometry{top=2.170cm,
%             bottom=3.510cm,
%             inner=2.1835cm,
%             outer=2.1835cm,
%             ignoremp}
% \part{Representing geographic information}
% \label{pa:representation}
% \restoregeometry%
% The underlying 3D simplicial complex in the representation of a cube as a 3D generalised map.

% \input{math}

% \input{modelling-background}

% \input{nd-modelling}

% \setpartpreamble{
%   \vspace{5cm}
%   \includegraphics[width=\linewidth]{figs/extrusion-steps}
% }
% \newgeometry{top=2.170cm,
%             bottom=3.510cm,
%             inner=2.1835cm,
%             outer=2.1835cm,
%             ignoremp}
% \part{Constructing and manipulating objects}
% \label{pa:operations}
% \restoregeometry%

% \input{operations-background}
% \input{extrusion}
% \input{incremental-construction}
% \input{linking-lods}
% \input{slicing}

% \bookmarksetup{startatroot}
% \addtocontents{toc}{\medskip\bigskip}

% \input{cleaning}

\cleardoublepage%
\renewpagestyle{scrheadings}{
  {\makebox[2em][r]{\thepage}\quad\rule{1pt}{100pt}\quad\leftmark}%
  {\hfill\rightmark\quad\rule{1pt}{100pt}\quad\makebox[2em][l]{\thepage}}%
  {}
}{
  {}%
  {}%
  {}
}
\renewpagestyle{plain.scrheadings}{
  {}%
  {}%
  {}
}{
  {\thepage}%
  {\hfill\thepage}%
  {}
}
\newgeometry{top=2.170cm,
            bottom=3.510cm,
            inner=2.1835cm,
            outer=2.1835cm,
            ignoremp}

\input{src/dictionary}

\addtocontents{toc}{\medskip\bigskip}

\backmatter%

% Bibliograhy
\bibliographystyle{plainnat}
{\small\bibliography{docs}}

\restoregeometry%

% Page style for back matter
\renewpagestyle{scrheadings}{
  {\hspace{-\marginparwidth}\hspace{-\marginparsep}\makebox[\overflowingheadlen][l]{\makebox[2em][r]{\thepage}\quad\rule{1pt}{100pt}\quad\leftmark}}%
  {\makebox[\overflowingheadlen][r]{\rightmark\quad\rule{1pt}{100pt}\quad\makebox[2em][l]{\thepage}}}%
  {}
}{
  {}%
  {}%
  {}
}
\renewpagestyle{plain.scrheadings}{
  {}%
  {}%
  {}
}{
  {\thepage}%
  {\makebox[\overflowingheadlen][r]{\thepage}}%
  {}
}

% \cleardoublepage%
% \label{thesis:back}
% \pdfbookmark[-1]{Back matter}{thesis:back}

% \input{summary}

% \input{cv}

% Back cover
% \clearpage
% \thispagestyle{empty}
% \null%
% \clearpage
% \includepdf{cover-back.pdf}

\end{document}
