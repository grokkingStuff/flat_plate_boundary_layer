% PDE terms
\newcommand{\pde}[2]{ \frac{\partial #1}{\partial #2} }
\newcommand{\pdetwo}[2]{ \frac{\partial^{2} #1}{\partial {#2}^{2} }}



\section{Simplifying Conservation Laws into Boundary Layer Equations}


\begin{marginfigure}
\caption[Symbols \& meaning in Navier-Stokes equations]{Symbols \& meaning in Navier-Stokes equations}
\label{tab:navier_stokes_symbols}
\small
\begin{tabular}{cc}
\toprule
Symbol & Meaning \\
\midrule
$x$    & horizontal coordinate \\
$y$    & vertical coordinate \\
$z$    & into-the-plane coordinate \\
$u$    & horizontal fluid velocity \\
$v$    & vertical fluid velocity \\
$w$    & into-the-plane fluid velocity \\
$\rho$ & fluid density \\
$g$    & gravity \\
$p$    & fluid pressure \\
\bottomrule
\end{tabular}
\end{marginfigure}


The fluid continuity equation describes the conversation of mass in differential form while the Navier-Stokes equations describe the conservation of momentum in differential form.

\begin{equation}
\pde{u}{t} + u\pde{u}{t} + v\pde{u}{y} + w\pde{u}{z} = -\frac{1}{\rho}\pde{p}{x} + g_{x} + \nu\left( \pdetwo{u}{x} + \pdetwo{u}{y} + \pdetwo{u}{z} \right) \label{eq:3d_navier_stokes_x} \\
\end{equation}

\begin{equation}
\pde{v}{t} + u\pde{v}{t} + v\pde{v}{y} + w\pde{v}{z} = -\frac{1}{\rho}\pde{p}{y} + g_{y} + \nu\left( \pdetwo{v}{x} + \pdetwo{v}{y} + \pdetwo{v}{z} \right) \label{eq:3d_navier_stokes_y} \\
\end{equation}

\begin{equation}
\pde{w}{t} + u\pde{w}{t} + v\pde{w}{y} + w\pde{w}{z} = -\frac{1}{\rho}\pde{p}{z} + g_{z} + \nu\left( \pdetwo{w}{x} + \pdetwo{w}{y} + \pdetwo{w}{z} \right) \label{eq:3d_navier_stokes_z} \\
\end{equation}

\begin{equation}
\pde{u}{x} + \pde{v}{y} + \pde{w}{z} = 0 \label{eq:3d_navier_stokes_continuity}
\end{equation}





By using suitable assumptions, equations (\ref{eq:3d_navier_stokes_x}, \ref{eq:3d_navier_stokes_y}, \ref{eq:3d_navier_stokes_z}, \ref{eq:3d_navier_stokes_continuity}) can be simplified.


\begin{itemize}
\item Steady state assumption

      A flow field is said to be a steady state when the flow at any position in space does not change in time. Any differential term based on time $t$ can be simplified to zero.

\begin{align*}
\pde{u}{t} &= 0 & \pde{v}{t} &= 0 \\
\pde{w}{t} &= 0 & \pde{p}{t} &= 0 \\
\end{align*}

\item Neglect gravity terms

      A flow field is said to be a steady state when the flow at any position in space does not change in time. Any differential term based on time $t$ can be simplified to zero.

\begin{align*}
\vec{g} &= 0 & \pde{g}{x} &= 0 \\
\pde{g}{y} &= 0 & \pde{g}{z} &= 0 \\
\end{align*}

\item Two-dimensional simplification

      While any real-life flowfield is three-dimensional, we can simplify our analysis by assuming that cross sections of the field are identical to each other with no transverse fluid velocity. In this analysis, we will be using the $x$-coordinate as the direction of the freestream velocity and $z$-coordinate as the height above the flate plate.

\begin{align*}
v &= 0 & \pde{v}{x} &= 0 \\
\pde{v}{y} &= 0 & \pde{v}{z} &= 0 \\
\pde{u}{y} &= 0 & \pde{w}{z} &= 0 \\
\pdetwo{v}{y} &= 0 & \pdetwo{v}{z} &= 0 \\
\pdetwo{u}{y} &= 0 & \pdetwo{w}{z} &= 0 \\
\end{align*}

\item Flow is parallel to the plate

      The flow is primarily parallel to the plate.

\begin{align*}
w &= 0 & \pde{w}{t} &= 0 \\
\pde{w}{x} &= 0 & \pde{w}{z} &= 0 \\
\pdetwo{w}{x} &= 0 & \pdetwo{w}{z} &= 0 \\
\end{align*}

\item Thin boundary layer assumption

      Prandtl's assumption of a thin boundary layer allows for his use of similarity parameters (non-dimensional analysis for flowsfields in which streamline patterns are geometrically similar).

\begin{align*}
x' &= \frac{ x }{ L         } & y' &= \frac{ y }{ \delta    } \\
u' &= \frac{ u }{ U         } & p' &= \frac{ p }{ \rho {U_{\infty}}^{2} } \\
\end{align*}

\end{itemize}



The above assumptions allow us to simplify \ref{eq:3d_navier_stokes_x}, \ref{eq:3d_navier_stokes_y}, \ref{eq:3d_navier_stokes_z}, and \ref{eq:3d_navier_stokes_continuity} to:

\begin{align*}
\cancel{\pde{u}{t}} + u\pde{u}{x} + \cancel{v\pde{u}{y}} + \cancel{w\pde{u}{z}} &= -\frac{1}{\rho}\pde{p}{x} + \cancel{g_{x}} + \nu\left( \pdetwo{u}{x} + \cancel{\pdetwo{u}{y}} + \pdetwo{u}{z} \right) \\
\Rightarrow u\pde{u}{t} &= -\frac{1}{\rho}\pde{p}{x} + \nu\pdetwo{u}{x} + \nu\pdetwo{u}{z}
\end{align*}

\begin{align*}
\cancel{\pde{v}{t}} + \cancel{u\pde{v}{x}} + \cancel{v\pde{v}{y}} + \cancel{w\pde{v}{z}} &= \cancel{-\frac{1}{\rho}\pde{p}{y}} + \cancel{g_{y}} + \nu\left( \cancel{\pdetwo{v}{x}} + \cancel{\pdetwo{v}{y}} + \cancel{\pdetwo{v}{z}} \right) \\
\Rightarrow 0 &= 0
\end{align*}

\begin{align*}
\pde{w}{t} + u\pde{w}{x} + \cancel{v\pde{w}{y}} + w\pde{w}{z} &= -\frac{1}{\rho}\pde{p}{z} + \cancel{g_{z}} + \nu\left( \pdetwo{w}{x} + \cancel{\pdetwo{w}{y}} + \pdetwo{w}{z} \right) \\
\Rightarrow \pde{w}{t} + u\pde{w}{t} + w\pde{w}{z} &= -\frac{1}{\rho}\pde{p}{z} + \nu\pdetwo{w}{x} + \nu\pdetwo{w}{z}
\end{align*}

\begin{align*}
\pde{u}{x} + \cancel{\pde{v}{y}} + \pde{w}{z} &= 0 \\
\Rightarrow \pde{u}{x} + \pde{w}{z} &= 0
\end{align*}



\section{Two-dimensional flow}
The full equation of motion for a two-dimensional flow is:


\begin{equation}
\frac{\partial u}{\partial x} + \frac{\partial v}{\partial y} = 0
\end{equation}


\begin{equation}
u\frac{\partial u}{\partial x} + v\frac{\partial u}{\partial x} = - \frac{1}{\rho}\frac{\partial p}{\partial x} + \nu \left( \frac{\partial^2 u}{ \partial x^2} + \frac{\partial^2 u}{\partial y^2} \right)
\end{equation}


\begin{equation}
u\frac{\partial v}{\partial x} + v\frac{\partial v}{\partial x} = - \frac{1}{\rho}\frac{\partial p}{\partial y} + \nu \left( \frac{\partial^2 v}{ \partial x^2} + \frac{\partial^2 v}{\partial y^2} \right)
\end{equation}


\begin{marginfigure}
\caption[Symbols \& meaning in Navier-Stokes equations]{Symbols \& meaning in Navier-Stokes equations}
\label{tab:navier_stokes_symbols}
\small
\begin{tabular}{cc}
\toprule
Symbol & Meaning \\
\midrule
$x$ & horizontal coordinate \\
$y$ & vertical coordinate \\
$u$ & horizontal fluid velocity \\
$v$ & vertical fluid velocity \\
$\nu$ & kinematic viscosity \\
$\rho$ & fluid density \\
$p$ & fluid pressure \\
\bottomrule
\end{tabular}
\end{marginfigure}


\section{Boundary Conditions of the Boundary Layer}

The boundary conditions at the outer edge of the boundary layer where it interfaces with the freestream velocity are:


\begin{equation}
u\Bigr|_{\substack{y=\delta}} = u_{\infty}
\end{equation}

\begin{equation}
p\Bigr|_{\substack{y=\delta}} = p_{\infty}
\end{equation}

In real boundary layers, there isn't a discontinuity between the boundary layer and the freestream velocity. The velocity gradient at the interface between the boundary layer and the freestream velocity is zero.


\begin{equation}
\frac{\partial u}{\partial x} \Bigr|_{\substack{y=\delta}} = 0
\end{equation}


\begin{equation}
u\Bigr|_{\substack{y=0}} = 0
\end{equation}

\footnote{This boundary condition corresponds to the no-slip condition.}



\section{Types of boundary layer thickness}

Fluid velocity is zero at the flat plate surface \footnote{No-slip condition} and is equal to the free-stream velocity at infinity away from the surface \footnote{Infinity away from the surface is best thought of as distance away from the plate at which the fluid is not significantly affected by the presence of the plate}.

\subsection{Boundary Thickness}

The boundary thickness, $\delta$, is defined as the height at which the particle's velocity is 95 \% or 99\% of the free stream velocity. \sidenote{As will be discussed later, there isn't a sharp discontinuity from the boundary layer to the freestream velocity, making an experimental determination of boundary layer thickness difficult.}

\begin{equation}
\delta = \int \mathrm d y
\end{equation}

\subsection{Momentum Thickness}

The momentum thickness describes the boundary layer in terms of momentum flux, making it useful in determining skin friction drag. \sidenote{skin friction drag is the viscous force that causes the loss of momentum. By slowing down the fluid (adding force in the $-x$-direction, the flat plate experiences a force in the $+x$-direction.)}

The momentum thickness represents the height of the free-stream flow which would be needed to make up the deficiency in momentum flux within the boundary layer due to the shear force at the surface.

The momentum thickness for an in-compressible boundary layer is given by:

\begin{align*}
\rho b  {U_{\infty}}^{2} \theta &= \rho b \int u \left( U_{\infty} - u \right) \mathrm d y \\
\theta &= \int \frac{u}{u_{\infty}} \left( 1 - \frac{u}{u_{\infty}} \right) \mathrm d y
\end{align*}


\section{Blasius Equation}

\begin{align*}
u\pde{u}{x} + w\pde{u}{z} = \nu\pdeterm
\rho b  {U_{\infty}}^{2} \theta &= \rho b \int u \left( U_{\infty} - u \right) \mathrm d y \\
\theta &= \int \frac{u}{u_{\infty}} \left( 1 - \frac{u}{u_{\infty}} \right) \mathrm d y
\end{align*}


In general, the laminar boundary layer equations will form a system  of  partial  differential  equations
which can, in principle, be solved numerically. But it is also possible in particular conditions to work
out the solution analytically in certain cases. In our case with an incompressible flow over a flat plate
at zero incidence we can derive the Blasius solution. The plate starts at      and extends parallel to
the  x-axis  and  will  have  a  semi-infinite  length.  The  free  stream  velocity  will  be  constant.  It  follows
from equation (2.41) that a constant    will result in a constant pressure at the edge of the boundary
layer. This will result in the x-component of the momentum equation for the boundary layer equations:
