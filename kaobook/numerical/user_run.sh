#!/bin/bash

yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "cannot $*"; }

export -f yell
export -f die
export -f try


FOAM_DIRECTORIES=$(ls -d)


extractVal()
{
    if [ -f "$2" ]
    then
        foamDictionary -entry "$1" -value "$2" | \
            sed -n '/(/,/)/{ s/[()]//g; /^ *$/d; p}' \
            > "$3"
    else
        # Or some other tag?
        echo "Not such file: $2" 1>&2
        echo "0" > "$3"
    fi
}

helperFunction()
{
    endTime=$(foamListTimes -latestTime)
    extractVal boundaryField.bottomWall.value "$endTime/Cx" Cx.
    extractVal boundaryField.bottomWall.value "$endTime/wallShearStress" tau.$$
    extractVal boundaryField.bottomWall.value "$endTime/yPlus" yPlus.$$

    echo "# ccx tau_xx tau_yy tau_zz y+" > profiles.dat
    paste -d ' ' Cx.$$ tau.$$ yPlus.$$ >> profiles.dat
}


runSimpleFoam()
{
    cd $1 && blockMesh && simpleFoam
}
export -f runSimpleFoam





HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Group 2 Fluids | Heriot Watt University Dubai"
TITLE="Turbulent Flat Plate"
MENU="Choose one of the following options:"

OPTIONS=(1 "Run kEpsilon models"
         2 "Run kOmegaSST models"
         3 "Run foamToVTK on all cases"
         4 "Extract information")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            echo "Run kEpsilon models"
            find . -mindepth 1 -maxdepth 1 -type d -exec echo '{}' \; | grep "kEpsilon" | parallel -k --progress runSimpleFoam
            ;;
        2)
            echo "Run kOmegaSST models"
            find . -mindepth 1 -maxdepth 1 -type d -exec echo '{}' \; | grep "kOmegaSST" | parallel -k --progress runSimpleFoam
            ;;
        3)
            echo "You chose Option 3"
            ;;
esac

exit 0

<< ////
# extractVal()
# {
#     if [ -f "$2" ]
#     then
#         foamDictionary -entry "$1" -value "$2" | \
#             sed -n '/(/,/)/{ s/[()]//g; /^ *$/d; p}' \
#             > "$3"
#     else
#         # Or some other tag?
#         echo "Not such file: $2" 1>&2
#         echo "0" > "$3"
#     fi
# }
#
#
# endTime=$(foamListTimes -latestTime)
# extractVal boundaryField.bottomWall.value "$endTime/Cx" Cx.
# extractVal boundaryField.bottomWall.value "$endTime/wallShearStress" tau.$$
# extractVal boundaryField.bottomWall.value "$endTime/yPlus" yPlus.$$
#
# echo "# ccx tau_xx tau_yy tau_zz y+" > profiles.dat
# paste -d ' ' Cx.$$ tau.$$ yPlus.$$ >> profiles.dat


find_case_directories


ls -d */


cd kEpsilon_10ms_grading_0100 && foamToVTK && cd ..
cd kEpsilon_10ms_grading_0300 && foamToVTK && cd ..
cd kEpsilon_10ms_grading_0500 && foamToVTK && cd ..
cd kEpsilon_10ms_grading_1000 && foamToVTK && cd ..
cd kEpsilon_10ms_grading_2000 && foamToVTK && cd ..
cd kEpsilon_10ms_grading_5000 && foamToVTK && cd ..
cd kEpsilon_18ms_grading_0100 && foamToVTK && cd ..
cd kEpsilon_18ms_grading_0300 && foamToVTK && cd ..
cd kEpsilon_18ms_grading_0500 && foamToVTK && cd ..
cd kEpsilon_18ms_grading_1000 && foamToVTK && cd ..
cd kEpsilon_18ms_grading_2000 && foamToVTK && cd ..
cd kEpsilon_18ms_grading_5000 && foamToVTK && cd ..
cd kEpsilon_22ms_grading_0100 && foamToVTK && cd ..
cd kEpsilon_22ms_grading_0300 && foamToVTK && cd ..
cd kEpsilon_22ms_grading_0500 && foamToVTK && cd ..
cd kEpsilon_22ms_grading_1000 && foamToVTK && cd ..
cd kEpsilon_22ms_grading_2000 && foamToVTK && cd ..
cd kEpsilon_22ms_grading_5000 && foamToVTK && cd ..
cd kOmegaSST_10ms_grading_0100 && foamToVTK && cd ..
cd kOmegaSST_10ms_grading_0300 && foamToVTK && cd ..
cd kOmegaSST_10ms_grading_0500 && foamToVTK && cd ..
cd kOmegaSST_10ms_grading_1000 && foamToVTK && cd ..
cd kOmegaSST_10ms_grading_2000 && foamToVTK && cd ..
cd kOmegaSST_10ms_grading_5000 && foamToVTK && cd ..
cd kOmegaSST_18ms_grading_0100 && foamToVTK && cd ..
cd kOmegaSST_18ms_grading_0300 && foamToVTK && cd ..
cd kOmegaSST_18ms_grading_0500 && foamToVTK && cd ..
cd kOmegaSST_18ms_grading_1000 && foamToVTK && cd ..
cd kOmegaSST_18ms_grading_2000 && foamToVTK && cd ..
cd kOmegaSST_18ms_grading_5000 && foamToVTK && cd ..
cd kOmegaSST_22ms_grading_0100 && foamToVTK && cd ..
cd kOmegaSST_22ms_grading_0300 && foamToVTK && cd ..
cd kOmegaSST_22ms_grading_0500 && foamToVTK && cd ..
cd kOmegaSST_22ms_grading_1000 && foamToVTK && cd ..
cd kOmegaSST_22ms_grading_2000 && foamToVTK && cd ..
cd kOmegaSST_22ms_grading_5000 && foamToVTK && cd ..



parallel -j 10 block ::: id1 id2 id3 id4 ....

////
