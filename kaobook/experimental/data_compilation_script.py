#!/usr/bin/env python
# coding: utf-8

# In[41]:


import pandas as pd  # excel but powerful and blindfolder
import numpy as np   # datatypes and number crunching
import os
import re
import glob


# In[42]:


# Example file name
# file_name = '21_07_26_18ms_080mm_0130mm.csv'
fpattern = r'{}_{}_{}_{}ms_{}mm_{}mm.csv'
file_names = glob.glob(fpattern.format('*','*','*','*','*','*'))
#print(files)


# In[53]:


series_list = []
for file_name in file_names:

    # Only read the useful columns, ignore timestamps
    col_list = ["Press(PSI)", "Press(Pa)", 
                "VEL(m/s)", "TEMP(C)", 
                "Flow(CMM)"]
    
    try:
        # Only read the useful columns
        # Ignore index and timestamps
        col_list = ["Press(Pa)", "VEL(m/s)", 
                    "TEMP(C)", "Flow(CMM)"] 
        data_df = pd.read_csv(file_name, 
                              # CSV files seem to love tabs 
                              # for something with comma in the name
                              delimiter='\t',
                              # Using utf-8 leads to codec error
                              encoding='utf-16',
                              # Only read the useful columns
                              # ignore timestamps
                              usecols=col_list)
    
    except:
        # Only read the useful columns
        # Ignore index and timestamps
        col_list = ["Press(PSI)", "VEL(m/s)", 
                    "TEMP(C)", "Flow(CMM)"] 
        data_df = pd.read_csv(file_name, 
                              # CSV files seem to love tabs 
                              # for something with comma in the name
                              delimiter='\t',
                              # Using utf-8 leads to codec error
                              encoding='utf-16',
                              # Only read the useful columns
                              # ignore timestamps
                              usecols=col_list)
        # Convert to Pa from PSI
        data_df["Press(PSI)"] = data_df["Press(PSI)"] * 6894.76
        # Rename the columns for user-friendliness
        data_df.rename({'Press(PSI)': 'Press(Pa)'},
                        axis=1, inplace=True)

    # Rename the columns for user-friendliness
    data_df.rename({'Press(Pa)': 'pressure', 
                    'VEL(m/s)': 'velocity',
                    'TEMP(C)': 'temperature',
                    'Flow(CMM)': 'flow'}, axis=1, inplace=True)

    # Average the columns
    data_df = data_df.mean()


    # Extract the following from the file name:
    # - freestream velocity
    # - distance from leading edge
    # - height of the pitot tube

    try:
        search = re.search('(\d\d)_(\d\d)_(\d\d)_(\d\d)ms_(\d\d\d)mm_(\d\d\d\d)mm', 
                      file_name)
    
        year = search.group(1)
        year = int(year)
    
        month = search.group(2) 
        month = int(month)
    
        day = search.group(3)
        day = int(day)
    
        speed = search.group(4)
        speed = int(speed)
    
        distance = search.group(5)
        distance = int(distance)
    
        height = search.group(6)    
        height = int(height)
    
        filename_pd = pd.Series([year, month, day, speed, distance, height], 
                                index=['year', 'month', 'day', 
                                       'speed', 'distance', 'height'])
    
    except AttributeError:    
        # apply your error handling
        raise ValueError("Regex search of filename failed :( ")
    
    data_df = data_df.append(filename_pd)

    series_list.append(data_df)
    
    
df = pd.concat(series_list, axis=1)
df = df.transpose()
print(df)


# In[ ]:




